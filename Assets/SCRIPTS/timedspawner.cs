﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timedspawner : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject spawne;
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawndelay;
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawndelay);
    }

    // Update is called once per frame
    public void SpawnObject()
    {
        Instantiate(spawne, transform.position, transform.rotation);

        if (stopSpawning)
        {
            CancelInvoke("SpawnObject");

        }

    }
}
