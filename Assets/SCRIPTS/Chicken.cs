﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : MonoBehaviour
{
    public GameObject ReplayMenu;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "enemies")
        {
            Destroy(gameObject);
            ReplayMenu.SetActive(true);
            Time.timeScale = 0f;
        }
    }
}
