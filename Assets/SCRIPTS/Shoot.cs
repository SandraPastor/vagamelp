﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Camera visor;

    public void Shooting()
    {
        Ray ray = visor.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            //Transform objectHit = hit.transform;
            if (hit.collider.CompareTag("enemies"))
            {
                if (hit.collider.gameObject.GetComponent<Enemies>().Health > 1)
                {
                    hit.collider.gameObject.SendMessage("TakenDamage");
                }

                else if (hit.collider.gameObject.GetComponent<Enemies>().Health == 1)
                {
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }
}
